# Get magnolia webapp
FROM alpine:3.10 AS builder
WORKDIR /app

# ARG MAGNOLIA_VERSION=6.1.2
# RUN mvn -C -B dependency:get \
#     -DremoteRepositories=https://nexus.magnolia-cms.com/content/repositories \
#     -Ddest=./ \
#     -Dartifact=info.magnolia.bundle:magnolia-community-demo-webapp:${MAGNOLIA_VERSION}:jar \
#     -Dtransitive=false \
#     -Ddest=magnolia.jar

# Maven, eat a buffet of dicks, seriously.
RUN apk add curl unzip && \
    curl -v https://nexus.magnolia-cms.com/content/repositories/magnolia.public.releases/info/magnolia/bundle/magnolia-community-demo-webapp/6.1.2/magnolia-community-demo-webapp-6.1.2.war --output magnolia.war

RUN ls -Fahl . && \
    mkdir magnolia/ && \
    unzip magnolia.war -d magnolia/

FROM busybox:latest AS final

COPY --from=builder /app/ /
